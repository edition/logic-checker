#include "statementwindow.h"
#include "ui_statementwindow.h"

StatementWindow::StatementWindow(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::StatementWindow)
{
    ui->setupUi(this);
}

StatementWindow::~StatementWindow()
{
    delete ui;
}
