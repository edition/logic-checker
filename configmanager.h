#ifndef CONFIGMANAGER_H
#define CONFIGMANAGER_H

#include <QXmlStreamReader>
#include <QString>
#include <QMap>

class ConfigManager
{
public:
    ConfigManager();
    ~ConfigManager();
    QString getSymbolByKey(QString k);
    static ConfigManager* pInstance();
private:
    QMap<QString, QString> _symbols;
};

#endif // CONFIGMANAGER_H
