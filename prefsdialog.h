#ifndef PREFSDIALOG_H
#define PREFSDIALOG_H

#include <QDialog>

namespace Ui {
class prefsdialog;
}

class prefsdialog : public QDialog
{
    Q_OBJECT

public:
    explicit prefsdialog(QWidget *parent = nullptr);
    ~prefsdialog();

private:
    Ui::prefsdialog *ui;
};

#endif // PREFSDIALOG_H
