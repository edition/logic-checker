#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "statementwindow.h"
#include "prefsdialog.h"
#include "configmanager.h"
#include <QMdiSubWindow>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    connect(ui->action_New_Window, &QAction::triggered, this, [&](){
        StatementWindow* sw = new StatementWindow(ui->mdiArea);
        sw->setWindowTitle(tr("Proposition"));
        ui->mdiArea->addSubWindow(sw)->resize(640,480);
        sw->show();
    });
    connect(ui->action_Options, &QAction::triggered, this, [&](){
        prefsdialog* pd = new prefsdialog(this);
        //Resume execution when the dialog is closed, and returns something
        pd->exec();
    });
}

MainWindow::~MainWindow()
{
    delete ui;
    delete ConfigManager::pInstance();
}
