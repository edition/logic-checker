#include "prefsdialog.h"
#include "ui_prefsdialog.h"
#include "configmanager.h"

prefsdialog::prefsdialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::prefsdialog)
{
    ui->setupUi(this);
    ui->leAnd->setText(ConfigManager::pInstance()->getSymbolByKey("and"));
    ui->leNot->setText(ConfigManager::pInstance()->getSymbolByKey("not"));
    ui->leOr->setText(ConfigManager::pInstance()->getSymbolByKey("or"));
    ui->leLParens->setText(ConfigManager::pInstance()->getSymbolByKey("left parenthesis"));
    ui->leRParens->setText(ConfigManager::pInstance()->getSymbolByKey("left parenthesis"));
    ui->leImplies->setText(ConfigManager::pInstance()->getSymbolByKey("implies"));
    ui->leTherefore->setText(ConfigManager::pInstance()->getSymbolByKey("therefore"));
}

prefsdialog::~prefsdialog()
{
    delete ui;
}
