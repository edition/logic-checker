#include "configmanager.h"
#include <QFile>
#include <QApplication>
#include <QDir>
#include <QDebug>

ConfigManager::ConfigManager()
{
    //check the file exists, if it doesn't copy it
    QString filename = QString("%1/config.xml").arg(qApp->applicationDirPath());
    if (!QFile::exists(filename)) {
        QFile::copy(":/data/config.xml", filename);
    }
    QFile file(filename);
    if(!file.open(QFile::ReadOnly | QFile::Text)){
        qDebug() << "Cannot read file " << file.errorString();
        exit(0);
    }
    QXmlStreamReader reader(&file);
    if (reader.readNextStartElement()) {
        if (reader.name() == "settings") {
            if (reader.readNextStartElement()) {
                if (reader.name() == "symbols") {
                    while (reader.readNextStartElement()) {
                        if (reader.name() == "symbol") {
                            //qDebug() << reader.attributes().value("name").toString() << "," << reader.readElementText();
                            QString name = reader.attributes().value("name").toString();
                            QString text = reader.readElementText();
                            _symbols.insert(name,text);
                        }
                    }
                }
            }
        }
    }
}

ConfigManager::~ConfigManager()
{

}

QString ConfigManager::getSymbolByKey(QString k)
{
    return _symbols.value(k);
}

ConfigManager* ConfigManager::pInstance()
{
    static ConfigManager* pConfigManager;
    if (!pConfigManager) {
        pConfigManager = new ConfigManager();
    }
    return pConfigManager;
}
