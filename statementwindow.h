#ifndef STATEMENTWINDOW_H
#define STATEMENTWINDOW_H

#include <QWidget>

namespace Ui {
class StatementWindow;
}

class StatementWindow : public QWidget
{
    Q_OBJECT

public:
    explicit StatementWindow(QWidget *parent = nullptr);
    ~StatementWindow();

private:
    Ui::StatementWindow *ui;
};

#endif // STATEMENTWINDOW_H
