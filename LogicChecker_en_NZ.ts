<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_NZ" sourcelanguage="en_NZ">
<context>
    <name>MainWindow</name>
    <message>
        <source>&amp;File</source>
        <translation></translation>
    </message>
    <message>
        <source>Windows</source>
        <translation></translation>
    </message>
    <message>
        <source>&amp;New Window</source>
        <translation></translation>
    </message>
    <message>
        <source>Ctrl+N</source>
        <translation></translation>
    </message>
    <message>
        <source>New W&amp;orkspace</source>
        <translation></translation>
    </message>
    <message>
        <source>&amp;Save Workspace</source>
        <translation></translation>
    </message>
    <message>
        <source>E&amp;xit</source>
        <translation type="unfinished">Salir</translation>
    </message>
    <message>
        <source>Logic Checker</source>
        <translation></translation>
    </message>
    <message>
        <source>Proposition</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>StatementWindow</name>
    <message>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <source>Vars</source>
        <translation></translation>
    </message>
    <message>
        <source>Truth Table</source>
        <translation></translation>
    </message>
    <message>
        <source>Syntax Tree</source>
        <translation></translation>
    </message>
</context>
</TS>
