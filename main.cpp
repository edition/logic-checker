#include "mainwindow.h"
#include "configmanager.h"
#include <QApplication>
#include <QTranslator>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    QTranslator appTranslator;
    appTranslator.load("LogicChecker_" + QLocale::system().name());
    a.installTranslator(&appTranslator);
    ConfigManager::pInstance();
    MainWindow w;
    w.show();
    return a.exec();
}
